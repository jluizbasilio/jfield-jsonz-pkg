<?php

namespace jluizbasilio\JFieldJsonZPkg;

class JFieldJsonZ
{
    public function __construct()
    {
        //
    }

    /**
     * METHODS VM
     */
    public function getVMFieldValue($field, $order, $idx = 0)
    {
        if (!$field)
        {
            return false;
        }

        if (!$order)
        {
            return false;
        }

        /**
        * A ideia é que ao usar o campo, possa ser definido algumas informações sobre o campo e o conteúdo.
        * Ex.: source("order","userinfo"...), tipo de campo custom se for o caso e conteúdo.
        */

        //Se o campo no JSON for configurado como SOURCE "Order"
        if ($field->source == "order")
        {

            if ($field->repeatable)
            {
                if ($field->content)
                {
                    //$output = new stdClass();
                    foreach ($field->content as $key => $value)
                    {
                        if ( is_array( $field->content->{$key} ) )
                        {
                            foreach ($field->content->{$key} as $pos => $item)
                            {
                                eval("\$fieldInner = ($\$key[\$item]);"); //$fieldInner representa o um array tipo o $order["items"] do VM

                                foreach ($fieldInner as $i => $inner)
                                {
                                    foreach ($field->fields as $f)
                                    {
                                        $output[$i][$f->name] = $this->getVMFieldValue($f, $inner);
                                    }
                                }

                            }
                        }
                    }
                    //print_r($output);
                    return $output;
                }
            }

            if ($order[$field->name])
            {
               return $order[$field->name];
            }
            else  if ($order['details']['BT']->{$field->name})
            {
                //Se for do tipo método de pagamento
                if ($field->name == "virtuemart_paymentmethod_id")
                {
                    $paymentModel = VmModel::getModel('paymentmethod');
                    $paymentmethod = $paymentModel->getPayment($order['details']['BT']->virtuemart_paymentmethod_id);

                    return $paymentmethod->payment_name;
                }

                //Se for do tipo método de envio
                else if ($field->name == "virtuemart_shipmentmethod_id")
                {
    				$shipmentModel= VmModel::getModel('shipmentmethod');
    				$shipmentmethod = $shipmentModel->getShipment($order['details']['BT']->virtuemart_shipmentmethod_id);

    				return $shipmentmethod->shipment_name;
                }
                else
                {
                    return $order['details']['BT']->{$field->name};
                }

            }
            else
            {
                return $order['items'][$idx]->{$field->name};
            }
        }

        //Se o campo no JSON for configurado como SOURCE "Customfield"
        else if ($field->source == "customfield")
        {
            if ($field->type == "multivariant") {
                //Se for um placeholder para multi variant
                /*
                if($field->store_field[0] == '{' && $field->store_field[strlen($field->store_field) - 1] == '}')
                {
                    return $this->getMultiVariantFieldValue($field->store_field,$order);
                }
                */
                return $this->getVMMultiVariantFieldValue($field, $order, $idx);
            }
            else //Se necessário aprofundo o tratamento, inicialmente será multi variant e não.
            {
                //Preciso buscar a relação de campos custom para o item do pedido.
                return $this->getVMCustomFieldValue($field->custom_id, $order, $idx);
            }
        }
    }

    /**
     *
     */
    function getVMCustomFieldValue($custom_id, $order, $idx = 0)
  	{
  		if (!$custom_id) { return false; }
  		if (!$order) { return false; }

        if ($order['items'][$idx])
        {
            $item = $order['items'][$idx];
  			$attributes = json_decode($item->product_attribute);
  			foreach ($item->customfields as $cf)
  			{
  				if ($cf->virtuemart_custom_id == $custom_id)
  				{
  					if ( is_array($attributes->{$cf->virtuemart_custom_id}) && ($cf->field_type != "C") )
  					{
  						return $cf->customfield_value;
  					}

  					if (is_object($attributes->{$cf->virtuemart_custom_id}))
  					{

  						foreach ($attributes->{$cf->virtuemart_custom_id} as $key => $value)
  						{
  							if (!is_object($attributes->{$cf->virtuemart_custom_id}->{$key}))
  							{
  								return $cf->customfield_value;
  							}

  							$content[] = $value->comment;
  						}

  						return implode(';', $content);
  						//return $cf->customfield_value;
  					}

  					//TALVEZ SEJA NECESSÁRIO MELHORAR O vmorderfields de forma que ele reconheça cada Label customizado do campo Multi Variant
  					/*
  					[selectoptions] => Array
            (
                [0] => stdClass Object
                    (
                        [voption] => clabels
                        [clabel] => Modalidade
                        [values] => Online
                    )

                [1] => stdClass Object
                    (
                        [voption] => clabels
                        [clabel] => Data de início do curso
                        [values] => Janeiro 2021 - Prova Ago / 2020
                    )
            )
  					*/
  					/*
  					[product_attribute] => {
  					    "100": [],
  					    "11": [],
  					    "96": "10073", //[virtuemart_customfield_id] => 10073
  					    "17": [],
  					    "94": {
  					        "10075": "Red" // tipo lista
  					    },
  					    "21": [] //Multi Variant - [selectoptions]
  					}
  					*/
  					/*
  					if ($cf->field_type == "C") //Multi Variant
  					{
  						[2283] => Array - ID do Produto filho comprado
              (
                  [0] => Online - 1º Label
                  [1] => Março 2021 - 2º Label
              )
  					}
  					*/
  					return $cf->customfield_value;
  				}
  			}
  		}
  	}

    /**
     *
     */
  	function getVMMultiVariantFieldValue($field, $order, $idx = 0)
  	{
  		if (!$field)
  		{
  			return false;
  		}

  		//$regrex = '/\{(.*?)\}/';

  		// Usa o REGEX:
  		//preg_match_all($regrex, $value, $resultado);

		if($order['items'][$idx])
		{
			$item = $order['items'][$idx];

			$attributes = json_decode($item->product_attribute);
			foreach ($item->customfields as $cf)
			{

				if ($cf->field_type != "C")
				{
					continue;
				}

				if ($cf->virtuemart_custom_id == $field->custom_id)
				{
					foreach ($cf->selectoptions as $key => $option)
					{
						if ($option->clabel == $field->clabel)
						{
							return $cf->options->{$item->virtuemart_product_id}[$key];
						}
					}
				}

			}
		}
		else
		{
			return false;
		}

  	}

    /**
     * METHODS EVENT BOOKING
     */

    function getEBFieldValue($field, $data)
    {
        //Se o campo no JSON for configurado como SOURCE "Order"
        if ($field->source == "data")
        {
            if ($field->repeatable)
            {
                if ($field->content)
                {
                    //$output = new stdClass();
                    foreach ($field->content as $key => $value)
                    {
                        if ( is_array( $field->content->{$key} ) )
                        {
                            foreach ($field->content->{$key} as $pos => $item)
                            {
                                eval("\$fieldInner = ($\$key[\$item]);"); //$fieldInner representa o um array tipo o $order["items"] do VM

                                foreach ($fieldInner as $i => $inner)
                                {
                                    foreach ($field->fields as $f)
                                    {
                                        $output[$i][$f->name] = $this->getEBFieldValue($f, $inner);
                                    }
                                }

                            }
                        }
                    }
                    //print_r($output);
                    return $output;
                }
            }
            //print_r("Campo não Repeatable [".$field->name."]");

            if (is_array($data))
            {
                return $data[$field->name];
            }
            else
            {
                return $data->{$field->name};
            }
        }

        //Caso seja src Row
        elseif ($field->source == "row")
        {
            // code...
        }

        //Caso seja src Mix
        elseif ($field->source == "mix")
        {
            if ($field->fields)
            {
                foreach ($field->fields as $f)
                {
                    $output[] = $this->getEBFieldValue($f, $data);
                }

                if ($field->operation == "concat")
                {
                    return implode(' ', $output);
                }
                else if ($field->operation == "soma")
                {
                    foreach ($output as $vl)
                    {
                        $tot += $vl;
                    }

                    return $tot;
                }
                else
                {
                    return false;
                }
            }
        }
        else
        {
            if (is_array($data))
            {
                //echo "<pre>É Array".print_r($field,1)."</pre>";
                if (!is_array($field) && !is_object($field))
                {
                    return $field;
                }
                foreach ($field as $k => $v)
                {
                    //echo "Var k => ".$k;
                    if (is_object($v))
                    {
                        $output[$k] = $this->getEBFieldValue($v, $data);
                    }
                    else
                    {
                        $output[$k] = $v;
                    }
                }

                //print_r($output);
                return $output;

            }
            else
            {
                foreach ($field as $k => $v)
                {
                    if (is_object($v))
                    {
                        $output[$k] = $this->getEBFieldValue($v, $data);
                    }
                    else
                    {
                        $output[$k] = $v;
                    }
                }

                //print_r($output);
                return $output;

            }
        }
    }

//END FILE
}

/**
 * //Verifico se o campo multi valor ta setado para comparar os resultados
 if ($field->multiple_value)
 {
     $data->{$field->sf_field} = $this->getMoreValuesRelation($field, $data->{$field->sf_field});
     continue;
 }
 */
